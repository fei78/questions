function handler() {
    var username = document.getElementById('username')
    var password = document.getElementById('password')
    fetch('./login.do', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: 'username=' + username.value + "&password=" + password.value
    }).then(resp => {
        if (!resp.ok) {
            alert("请求失败！")
            return
        }
        return resp.json()
    }).then(data => {
        if (data.ok) {
            location.replace('./profile.html')
        } else {
            alert('登录失败！')
        }
    })
}
window.addEventListener('DOMContentLoaded', () => {
    var submitbtn = document.getElementById('submitbtn')
    submitbtn.addEventListener('click', handler)
})
